__author__ = 'Guillaume'


#!/usr/bin/python2

import time
import numpy as np

# Init OSC
from lib.Exemple_simple_OSC import OSC


def simulate_ableton_OSC_messages(duration=10.,FS=100.,send_to_ip='127.0.0.1',send_to_port=5004):
    client = OSC.OSCClient()
    frq_tempo_chg = 2.

    tempo = 120.
    volume = 0.
    beat = 0
    msg_volume = OSC.OSCMessage("/volume")
    msg_volume.append(volume)
    msg_tempo = OSC.OSCMessage("/tempo")
    msg_tempo.append(tempo)
    msg_beat = OSC.OSCMessage("/beat")
    msg_beat.append(beat)

    beat_t = 0.
    for i in range(int(FS * duration)):
        time.sleep(1/FS)
        t = i / FS
        dbeat = 1/FS * tempo / 60.
        beat_t += dbeat
        if int(beat_t) < beat_t and int(beat_t) > beat_t - dbeat:
            msg_beat[0] = int(beat_t)
            print 'Beat: {0}'.format(int(beat_t))
            try:
                client.sendto(msg_beat, (send_to_ip, send_to_port))
            except:
                print 'Connection refused'

        rd = np.random.rand(1)
        if np.random.rand(frq_tempo_chg/FS > rd):

            dtempo = np.random.randn(1) /30
            tempo += dtempo*tempo


            print 'Tempo: {0}'.format(tempo)
            msg_tempo[0] = tempo
            try:
                client.sendto(msg_tempo, (send_to_ip, send_to_port))
            except:
                print 'Connection refused'

        # Ramp with the volume
        msg_volume[0] = t*2/duration - int(t*2/duration)
        try:
            client.sendto(msg_volume, (send_to_ip, send_to_port))
        except:
            print 'Connection refused'

#simulate_ableton_OSC_messages()
