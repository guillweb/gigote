__author__ = 'Guillaume'

from src.generic.connector import *
from src.generic.osc_API import *
from osc_acc_API import *
from osc_live_API import *

class AccToLiveConnector(Connector):

    def __init__(self,acc_ip,acc_port,live_ip,live_port,out_port,reading_FS=20,recording_FS=1000.,predict_online=False,record=True,session_name='test_AccToLive',user_name="default"):

        self.recording_FS = recording_FS
        self.reading_FS = reading_FS

        self.cmd_file_type = 'acc'
        self.out_file_type = 'osc'

        # Receive data
        self.init_variables()
        self.init_reception(OSCreceiver(acc_ip,acc_port),OSCreceiver(live_ip,live_port))
        self.init_data_reading(OscAccVectoriser(reading_FS),OscLiveVectoriser(reading_FS))

        # For recording of the performance or training create vectorisers and recorders
        if predict_online:
            self.init_online_prediction(OscAccVectoriser(recording_FS),OscLiveVectoriser(recording_FS),OSCsender(live_ip,out_port))

        if record:
            self.init_recording(session_name,user_name)


        K_quantize = 7
        M_freq_list = 5
        n_full = 8 # this is seven because is does not count the expansion
        channel_to_exp = [0,1,2]
        n = len(channel_to_exp)
        tau_max = 1.
        tau_min = 2. / reading_FS

        #expander = SeparableScatExpander(n_full,tau_max,M_freq_list,tau_max,tau_min,to_expand_channel_list=channel_to_exp)
        #expander = FrequencyExpander(n_full,M_freq_list,tau_max,tau_min,mem_ratio=2,to_expand_channel_list=channel_to_exp)
        #expander = AccPreprocesser(n_full,tau_max,tau_min,M_freq_list,channel_to_exp)
        expander = SpatialQuantizer(n_full,K_quantize,to_expand_channel_list=channel_to_exp)
        self.init_classification(SGDRegressor(),expander)
