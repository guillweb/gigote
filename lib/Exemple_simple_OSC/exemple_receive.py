#!/usr/bin/python2

import threading

#------OSC Server-------------------------------------#
import OSC
import socket

receive_address = '192.168.173.1', 8000

# OSC Server. there are three different types of server. 
s = OSC.ThreadingOSCServer(receive_address)
#s = OSC.OSCServer(receive_address)
#s = OSC.OSCServer()

# this registers a 'default' handler (for unmatched messages)
s.addDefaultHandlers()

# define a message-handler function for the server to call.
def printing_handler(addr, tags, stuff, source):
    if addr=='/test':
        print "Test", stuff

    if addr=='/accelerometer':
        print "accelerometer", stuff


s.addMsgHandler("/test", printing_handler)
s.addMsgHandler("/accelerometer", printing_handler)

def main():
    # Start OSCServer
    print "Starting OSCServer"
    st = threading.Thread(target=s.serve_forever)
    st.start()

main()
