__author__ = 'Guillaume'

import argparse
import math
import numpy as np
import threading
import lib.OSC as OSC
import socket
from lib.OSC import decodeOSC
from src.generic.file_recording_management import Vectoriser
import time

class OSCreceiver(object):
    def __init__(self,ip,port,bf=1024):
        self.ip = ip
        self.port = port
        self.buffer_size = bf

        # Connect
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


        try:
            self.socket.bind((self.ip, self.port))
            self.socket.setblocking(0)
            self.socket.settimeout(0.002)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, self.buffer_size)
            print 'Plug : IP = ', self.ip,  'Port = ', self.port,  'Buffer Size =', self.buffer_size
        except:
            print 'No connected'

    def get_meta(self):
        obj = {"ip": self.ip,"port": self.port}
        return obj

    def get_data(self):
        try:
            raw_data = self.socket.recv(self.buffer_size)
            osc_data = OSC.decodeOSC(raw_data)
            if len(osc_data) == 3:
                data = {osc_data[0] : osc_data[2]}
            else:
                data = {osc_data[0] : osc_data[2:]}

            return data
        except socket.error: return False

class OSCsender(object):

    def __init__(self,ip,port):
        self.ip = ip
        self.port = port
        self.client = OSC.OSCClient()

    def send_data(self,address,value):
        msg = OSC.OSCMessage()
        msg.setAddress(address)

        msg.append(value)
        try:
            self.client.sendto(msg, (self.ip, self.port))
            msg.clearData()
        except:
            print 'Connection refused for OSC submission to {0}:{1} with msg: {2} {3}'.format(self.ip,self.port,address,value)
            pass
