__author__ = 'Guillaume'

import argparse
import math
import numpy as np
import threading
from src.generic.file_recording_management import Vectoriser
import time

class OscLiveVectoriser(Vectoriser):

    def init_io(self):
        self.n_inputs = 5
        self.n_outputs = 1

        self.default_input_vector = [2,1,0,1,0]
        self.default_output_vector = [0.]

    # ----------------------------
    # Treat beat and bar phases
    # ----------------------------
    def rot_matrix(self,theta):
            return [[np.cos(theta),-np.sin(theta)],[np.sin(theta),np.cos(theta)]]

    def iterate_beat(self,beat,t_last,t):
        """
        Specific iteration function for current beat entry.

        :param beat:
        :param t_idx:
        :return:
        """
        t_idx = self.get_time_idx(t)
        dt = t - t_last

        idx = min(t_idx,np.shape(self.input)[1]-1)
        rate = self.input[0,idx] # In online mode input is length 1
        theta = 2 * np.pi * dt * rate
        return np.dot(self.rot_matrix(theta),beat)

    def iterate_bar(self,beat,t_last,t):
        """
        Specific iteration function for current bar entry.

        :param beat:
        :param t_idx:
        :return:
        """
        t_idx = self.get_time_idx(t)
        dt = t - t_last

        idx = min(t_idx,np.shape(self.input)[1]-1) # In online mode input is length 1
        rate = self.input[0,idx] / 4.
        theta = 2 * np.pi * dt * rate
        return np.dot(self.rot_matrix(theta),beat)

    def assign_phases(self):
        '''
        Get the current beat or bar phase from the past of the vectorised vector.
        Record phases from last recorded update to current time.
        :param t: current time in second
        '''
        self.assign_values(self.input,1,2,self.iterate_beat)
        self.assign_values(self.input,3,2,self.iterate_bar)

    def vectorise_input(self,obj):
        '''
        Update input vector and insert in the input from the JSON decoded object
        :param obj: JSON decoded object from file
        '''


        if 'data' in obj:

            t_idx = self.get_time_idx(obj['t'])

            #Tempo
            self.assign_values(self.input,0,1,self.iterate_copy)
            if '/tempo' in obj['data']:
                self.input[0,t_idx] = obj['data']['/tempo']/60

            #Beat phase
            self.assign_phases()
            if '/currentBeat' in obj['data']:
                self.input[1:3,t_idx] = np.array([1,0])
                th = np.pi/2 * int(obj['data']['/currentBeat']) % 4
                self.input[3:5,t_idx] = np.dot(self.rot_matrix(th),np.array([1,0]))
        else:
            self.assign_phases()

    def vectorise_output(self,obj):
        '''
        Update output vector and insert in the input from the JSON decoded object
        :param obj: JSON decoded object from file
        '''
        if 'data' in obj:
            t_idx = self.get_time_idx(obj['t'])

            self.assign_values(self.output,0,1,self.iterate_copy)
            if '/volume' in obj['data']:
                self.output[0,t_idx] = obj['data']['/volume']
