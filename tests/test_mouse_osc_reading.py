__author__ = 'Guillaume'

import src.generic.file_recording_management as f_mgmt
import src.mouse_to_live.osc_live_API as osc_API
import src.mouse_to_live.mouse_API as mouse_API
import os

def test_mouse_osc_reading(name,root_path='sessions',FS=20):

    osc_vectoriser = osc_API.OscLiveVectoriser(FS)
    osc_file_name = root_path + '/' + name + '/' + name + '.osc'

    mouse_vectoriser = mouse_API.MouseVectoriser(FS)
    mouse_file_name = root_path + '/' + name + '/' + name + '.mouse'

    reader = f_mgmt.Reader()
    reader.init_time_vector_size(osc_vectoriser,osc_file_name,mouse_file_name)
    reader.init_time_vector_size(mouse_vectoriser,osc_file_name,mouse_file_name)

    reader.vectorise_from_file(osc_file_name,osc_vectoriser)
    reader.vectorise_from_file(mouse_file_name,mouse_vectoriser)

    print "Input vector OSC:"
    print osc_vectoriser.input
    print "Output vector OSC:"
    print osc_vectoriser.output

    print "Input vector mouse:"
    print mouse_vectoriser.input
    print "Output vector mouse:"
    print mouse_vectoriser.output

#name = 'test_MouseToLive00019'
#name = 'test_MouseToLive00032'
#test_mouse_osc_reading(name)