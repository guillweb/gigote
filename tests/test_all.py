__author__ = 'Guillaume'

import time
import threading
from threading import Thread
from simulate_OSC_from_ableton import  simulate_ableton_OSC_messages
from test_mouse_osc_reception import test_mouse_osc_reception
from test_mouse_osc_reading import test_mouse_osc_reading
from test_mouse_osc_class import test_mouse_osc_class
from test_mouse_osc_train_predict import test_mouse_osc_train_predict

test_choice = 1    # 0: all
                    # 1: Skip reception
                    # 2: Skip reception and classification

if test_choice <= 0:
    t1 = Thread(target = simulate_ableton_OSC_messages).start()
    t2 = Thread(target = test_mouse_osc_reception).start()

    print '-------------------------------'
    time.sleep(12.)
    print 'Simulation and Reception done in parallel.'

# Off line classification, make sure that there are no file previously defined for a good test
# empty sessions folder
name = 'test_MouseToLive00000'

if test_choice <= 1:
    test_mouse_osc_class(name,do_print=True)
    print 'Classification offline done.'

if test_choice <= 2:

    t1 = Thread(target = simulate_ableton_OSC_messages).start()
    t2 = Thread(target = test_mouse_osc_train_predict,args=(name,)).start()