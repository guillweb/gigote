__author__ = 'Guillaume'

import src.generic.file_recording_management as f_mgmt
import src.mouse_to_live.osc_live_API as osc_API
import src.mouse_to_live.mouse_API as mouse_API
import os

from sklearn import linear_model
import numpy as np

from src.mouse_to_live.connection import MouseToLiveConnector

def test_mouse_osc_train_predict(name="",root_path="sessions",reading_FS=10.,recording_FS=1000.,duration=10.,reception_ip='127.0.0.1',port_ip=5004,out_port=5005,partial_fit=True):
    connector = MouseToLiveConnector(reception_ip,port_ip,out_port=out_port,predict_online=True,record=True,reading_FS=reading_FS,recording_FS=recording_FS)

    if name == "":
        connector.train_from_files(root_path=root_path,do_print=True,partial=partial_fit)
    else:
        connector.train_from_file(name,root_path=root_path,do_print=True,partial=partial_fit)
    connector.serve(duration)