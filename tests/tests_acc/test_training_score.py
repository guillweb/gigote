__author__ = 'Guillaume'

from src.accelerometer_to_live.connection import AccToLiveConnector
from classification_tools import *
from config import *


# ------------------------------------
# SCRIPT FOR PRINTING A TRAINING SCORE

connector = AccToLiveConnector(acc_ip,acc_port,live_ip,live_port,out_port,
                               reading_FS=reading_FS,recording_FS=recording_FS,
                               predict_online=False,record=False,
                               session_name=session_name,
                               user_name=user_name)

names = get_file_list(root_path)
Ts,Xs,Ys = connector.train_from_file_list(names[0:N_files_train],root_path,do_print=False,partial=partial_fit)
scores = np.zeros(N_files_test+N_files_train)

N = N_files_test+N_files_train

# Compute train set scores
for k in range(N_files_train):
    Y_predict = connector.predict_offline(Ts[k],Xs[k])
    scores[k] = get_score(Ys[k],Y_predict)

# Compute training set scores
for k in range(N_files_test):
    T,X,Y = connector.load_data(names[N_files_train + k],root_path=root_path,do_print=False)
    Y_predict = connector.predict_offline(T,X)
    plt.plot(T,Y.T,linestyle='-')
    plt.plot(T,Y_predict,linestyle='--')

    scores[N_files_train +k] = get_score(Y,Y_predict)

print "Correlation scores on training set: {0}".format(scores[0:N_files_train])
print "Correlation scores on test set: {0}".format(scores[N_files_train:N])
plt.show()

