__author__ = 'Guillaume'

from src.mouse_to_live.connection import MouseToLiveConnector

def test_mouse_osc_reception(duration=10.,FS=100.,ip='127.0.0.1',port=5004,out_port=5005):
    connector = MouseToLiveConnector(ip,port,out_port,FS,predict_online=False,record=True)
    connector.serve(duration)