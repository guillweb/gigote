__author__ = 'Guillaume'


from src.accelerometer_to_live.connection import AccToLiveConnector
import os
import matplotlib.pyplot as plt

from sklearn import linear_model
import numpy as np

def get_file_list(root_path):
    names_full = os.listdir(root_path)
    names = []
    for name in names_full:
        if name != '.' and name != '..':
            names.append(name)

    return names

def get_score(Y,Y_predict):


    return np.mean((Y_predict - Y) ** 2)

