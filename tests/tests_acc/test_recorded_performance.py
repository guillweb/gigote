__author__ = 'Guillaume'

from src.accelerometer_to_live.connection import AccToLiveConnector
from classification_tools import *
from config import *

# ----------------------------------
# SCRIPT FOR RECORDING AND PERFORM

connector = AccToLiveConnector(acc_ip,acc_port,live_ip,live_port,out_port,
                               reading_FS=reading_FS,recording_FS=recording_FS,
                               predict_online=True,record=True,
                               session_name=session_name,
                               user_name=user_name)

names = get_file_list(root_path)
connector.train_from_file_list(names[0:N_files_train],root_path,do_print=False,partial=partial_fit)
connector.serve(duration)