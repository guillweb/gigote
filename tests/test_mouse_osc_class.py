__author__ = 'Guillaume'

import src.generic.file_recording_management as f_mgmt
import src.mouse_to_live.osc_live_API as osc_API
import src.mouse_to_live.mouse_API as mouse_API

from src.mouse_to_live.connection import MouseToLiveConnector
import os
import matplotlib.pyplot as plt

from sklearn import linear_model
import numpy as np


from profilehooks import profile

@profile
def test_mouse_osc_class(name,root_path='sessions',FS=100,do_print=False):

    reception_ip = '0.0.0.0'
    port_ip= '0000'
    connector = MouseToLiveConnector(reception_ip,port_ip,out_port=port_ip,predict_online=True,record=False, reading_FS=FS)
    T,X,Y = connector.train_from_file(name,root_path=root_path,do_print=do_print)
    Y_predict = connector.predict_offline(X)

    if do_print:
        plt.plot(T,Y.T,linestyle='-',label='Real volume')
        plt.plot(T,Y_predict,linestyle='--',label='Predicted volume')
        plt.legend()
        plt.show()

    pass

@profile
def test_mouse_osc_class_multi_files(root_path='sessions',FS=100,do_print=False,partial_fit=True):

    reception_ip = '0.0.0.0'
    port_ip= '0000'
    connector = MouseToLiveConnector(reception_ip,port_ip,out_port=port_ip,predict_online=True,record=False, reading_FS=FS)

    names_full = os.listdir(root_path)
    names = []
    for name in names_full:
        if name != '.' and name != '..':
            names.append(name)

    T_list,X_list,Y_list = connector.train_from_file_list(names,root_path=root_path,do_print=do_print,partial=partial_fit)

    if do_print:
        l = len(X_list)
        for k in range(min(len(X_list),3)):
            X = X_list[l-1-k]
            Y = Y_list[l-1-k]
            T = T_list[l-1-k]
            Y_predict = connector.predict_offline(T,X)
            plt.plot(T,Y.T,linestyle='-')
            plt.plot(T,Y_predict,linestyle='--')
        plt.show()

    pass