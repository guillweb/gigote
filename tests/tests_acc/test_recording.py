__author__ = 'Guillaume'

from src.accelerometer_to_live.connection import AccToLiveConnector
from classification_tools import *
from config import *

# ----------------------------------
# SCRIPT FOR RECORDING

connector = AccToLiveConnector(acc_ip,acc_port,live_ip,live_port,out_port,predict_online=False,record=True)
connector.serve(duration)