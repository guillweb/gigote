__author__ = 'Guillaume'

#----------------
# Session meta data
session_name = 'test_AccToLive'
user_name = 'Guillaume'
root_path = 'sessions'

#----------------
# Data parameters
N_files_train = 2
N_files_test = 2

recording_FS = 200.
reading_FS = 200.
duration = 78.
partial_fit = False

#----------------
# Address of the OSC server
acc_ip = '192.168.173.1'
acc_port = 8000

# Adress of the Max of Live OSC server
live_ip = '127.0.0.1'
live_port = 5004
out_port = 5005

