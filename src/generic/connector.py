__author__ = 'Guillaume'

from math import *
from session_management import *
from file_recording_management import *
import matplotlib.pyplot as plt

import lib.OSC as OSC
import socket
from lib.OSC import decodeOSC

from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.linear_model.stochastic_gradient import SGDRegressor
import numpy as np
import lib.OSC
from profilehooks import profile



class Connector(object):

    def init_variables(self):
        self.record = False
        self.predict_online = False

    def init_reception(self,cmd_receiver,out_receiver):

        # Receive data
        self.out_receiver = out_receiver
        self.cmd_receiver = cmd_receiver

    def init_data_reading(self,r_cmd_vectoriser,r_out_vectoriser):

        self.reading_out_vectoriser = r_out_vectoriser
        self.reading_cmd_vectoriser = r_cmd_vectoriser

    def init_online_prediction(self,cmd_vectoriser,out_vectoriser,out_sender):

        self.t = 0.
        self.t_last = 0.

        self.predict_online = True

        self.out_vectoriser = out_vectoriser
        self.cmd_vectoriser = cmd_vectoriser
        self.out_sender = out_sender

    def init_recording(self,session_name,user_name):

        self.do_record = True

        self.out_recorder = Recorder()
        self.cmd_recorder = Recorder()


        self.session_name = session_name
        self.user_name = user_name
        self.new_session(session_name,user_name,self.cmd_file_type,self.out_file_type)

    def init_classification(self,classifier,expander):
        self.classifier = classifier
        self.expander = expander
        self.out_scaler = Scaler(self.expander.n_out + self.expander.n_full - self.expander.n)

    def new_session(self,session_name,user_name,cmd_type,out_type):
        s_type = '[' + out_type + ',' + cmd_type + ']'
        self.session = Session(session_name,s_type,user_name)
        self.session.sync(self.out_receiver, self.out_recorder, out_type)
        self.session.sync(self.cmd_receiver,self.cmd_recorder, cmd_type)
        self.session.save()

    def set_t(self,t):
        self.t_last = self.t
        self.t = t

        self.cmd_vectoriser.t_last = self.cmd_vectoriser.t
        self.out_vectoriser.t_last = self.out_vectoriser.t

        self.cmd_vectoriser.t = t
        self.out_vectoriser.t = t

    @profile
    def train_from_file_list(self,names,root_path='sessions',do_print=False,partial=True):
        """
        Train from multiple recordings.
        It uses the function partial_fit of the scikit-learn regressor: SGDRegressor or the fit function that can be train only from one vector

        :param names:
        :param root_path:
        :param do_print:
        :param partial: Do partial training for huge dataset
        :return:
        """
        Ts = []
        Xs = []
        Ys = []

        for name in names:
            T,X,Y = self.load_data(name,root_path,do_print)

            if partial:
                self.train_expander_classifier(T,X,Y,do_print,partial)
            else:
                # Train the expander on an online way and the rest in a Batch way
                # TODO: Cleaning this
                self.expander.partial_learn_scaling(X,t_mat=T)
                XX = self.expander.expand_data_offline(X,t_mat=T)
                self.out_scaler.partial_learn_scaling(XX)
                XX = self.out_scaler.scale(XX)
                if len(Ts) == 0:
                    # Init the contactenation
                    X_traing_set,Y_training_set = XX,Y
                else:
                    X_traing_set = np.hstack((X_traing_set,XX))
                    Y_training_set = np.hstack((Y_training_set,Y))


            Ts.append(T)
            Xs.append(X)
            Ys.append(Y)

        if not(partial):
            self.classifier.fit(X_traing_set.T,Y_training_set.flatten())


        return Ts,Xs,Ys

    def train_from_file(self,name,root_path='sessions',do_print=False,partial=True):
        T,X,Y = self.load_data(name,root_path,do_print)
        self.train_expander_classifier(T,X,Y,do_print,partial)

        return T,X,Y

    def train_expander_classifier(self,T,X,Y,do_print=False,partial=True):
        """
        Train the classifier that will classify along the output
        :param
        :return
        """

        self.expander.partial_learn_scaling(X,t_mat=T)
        XX = self.expander.expand_data_offline(X,t_mat=T)
        self.out_scaler.partial_learn_scaling(XX)
        XX = self.out_scaler.scale(XX)

        if partial:
            self.classifier.partial_fit(XX.T,Y.flatten())
        else:
            self.classifier.fit(XX.T,Y.flatten())

        if do_print:
            # The mean square error
            print("Residual sum of squares: %.2f"
                  % np.mean((self.predict_offline(T,X) - Y.T) ** 2))
            # Explained variance score: 1 is perfect prediction

        return X,Y

    def predict_offline(self,T,x_mat_full):
        XX = self.expander.expand_data_offline(x_mat_full,t_mat=T)
        XX = self.out_scaler.scale(XX)

        return self.classifier.predict(XX.T)

    def diffuse_and_predict_online(self,x,t,t_last):

        X = self.expander.expand_and_diffuse_online(x,t,t_last)
        X = self.out_scaler.scale(X)

        return X,self.classifier.predict(X.T).T

    def load_data(self,name,root_path='sessions',do_print=False):

        reading_out_vectoriser = self.reading_out_vectoriser
        out_file_name = root_path + '/' + name + '/' + name + '.' + self.out_file_type

        reading_cmd_vectoriser = self.reading_cmd_vectoriser
        cmd_file_name =  root_path + '/' + name + '/' + name + '.' + self.cmd_file_type

        reader = Reader()
        reader.init_time_vector_size(reading_out_vectoriser,out_file_name,cmd_file_name)
        reader.init_time_vector_size(reading_cmd_vectoriser,out_file_name,cmd_file_name)

        reader.vectorise_from_file(out_file_name,reading_out_vectoriser)
        reader.vectorise_from_file(cmd_file_name,reading_cmd_vectoriser)

        X = reading_cmd_vectoriser.input
        X = np.vstack((X,reading_out_vectoriser.input))


        Y = reading_cmd_vectoriser.output
        Y = np.vstack((Y,reading_out_vectoriser.output))

        if do_print:
            print "Input vector OSC:"
            print reading_out_vectoriser.input
            print "Output vector OSC:"
            print reading_out_vectoriser.output

            print "Input vector mouse:"
            print reading_cmd_vectoriser.input
            print "Output vector mouse:"
            print reading_cmd_vectoriser.output
        return reading_out_vectoriser.T,X,Y

    def serve(self,duration,do_print=False):
        """
        Run a server that captures both mouse and OSC data.
        If predict is true, it will predict at each time step an output, i.e. a volume to send to ableton for performing live.
        :param duration
        :param predict_online
        """

        t0 = time.time()
        t = 0

        # Init online prediction
        if self.predict_online:

            self.cmd_vectoriser.init_online(t0)
            self.out_vectoriser.init_online(t0)
            self.set_t(t)

            x = self.cmd_vectoriser.input
            x = np.vstack((x,self.out_vectoriser.input))

            # Init temporary X expanded vector
            self.expander.expand_data_offline(x,np.array([0]))


        print 'Start serving: ' + self.session_name + '(' + self.user_name + ')'
        while t < duration:
            # update time stamp in vectoriser
            t = time.time() - t0
            obj_cmd = {'t': t}
            obj_out = {'t': t}

            # get and record mouse data
            cmd_data = self.cmd_receiver.get_data()
            if cmd_data: obj_cmd['data'] = cmd_data

            out_data = self.out_receiver.get_data()
            if out_data: obj_out['data'] = out_data

            # update state vector for instance classification
            if self.do_record:
                self.cmd_recorder.record_data(obj_cmd)
                self.out_recorder.record_data(obj_out)

            if self.predict_online:
                self.set_t(t)
                self.cmd_vectoriser.vectorise_input(obj_cmd)
                self.out_vectoriser.vectorise_input(obj_out)

                x = self.cmd_vectoriser.input
                x = np.vstack((x,self.out_vectoriser.input))

                X,Y = self.diffuse_and_predict_online(x,self.t,self.t_last)
                self.out_sender.send_data("/cmd",Y)

                if do_print:
                    print 'dt: {0},X: {1},Y: {2}'.format(t-self.t_last,X.flatten(),Y)

                Y_real = self.cmd_vectoriser.output
                Y_real = np.vstack((Y_real,self.out_vectoriser.output))


            time.sleep(1. / self.recording_FS)

        if self.do_record:
            self.out_recorder.file.close()
            self.cmd_recorder.file.close()

        print "Stop serving."
