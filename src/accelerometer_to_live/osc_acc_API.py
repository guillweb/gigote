__author__ = 'Guillaume'

import argparse
import math
import numpy as np
import threading
from src.generic.file_recording_management import Vectoriser
import time

class OscAccVectoriser(Vectoriser):

    def init_io(self):
        self.n_inputs = 3
        self.n_outputs = 0

        self.default_input_vector = [0,0.,0.]
        self.default_output_vector = []


    def vectorise_input(self,obj):
        '''
        Update input vector and insert in the input from the JSON decoded object
        :param obj: JSON decoded object from file
        '''


        if 'data' in obj:

            t_idx = self.get_time_idx(obj['t'])

            # For offline vectorisation, copy last value until each  time t
            self.assign_values(self.input,0,3,self.iterate_copy)

            # Update time t, in online mod t_idx is 0
            if '/accelerometer' in obj['data']:
                self.input[:,t_idx] = obj['data']['/accelerometer']


    def vectorise_output(self,obj):
        pass