__author__ = 'Guillaume'

import time
import threading
from threading import Thread
from simulate_OSC_from_ableton import  simulate_ableton_OSC_messages
from test_mouse_osc_reception import test_mouse_osc_reception
from test_mouse_osc_reading import test_mouse_osc_reading
from test_mouse_osc_class import *
from test_mouse_osc_train_predict import test_mouse_osc_train_predict



test_choice = 1     # 0: all
                    # 1: Skip reception
                    # 2: Skip reception and classification

recording_FS = 1000.
reading_FS = 100.
duration = 78.

partial = False

if test_choice <= 0:
    test_mouse_osc_reception(duration=duration,FS=recording_FS)

# Off line classification, make sure that there are no file previously defined for a good test
# empty sessions folder
name = 'test_MouseToLive00000'

if test_choice <= 1:
    test_mouse_osc_class_multi_files(do_print=True,FS=reading_FS,partial_fit=partial)
    print 'Classification offline done.'

if test_choice <= 2:
    test_mouse_osc_train_predict(name,duration=duration,recording_FS=recording_FS,reading_FS=reading_FS,partial_fit=partial)