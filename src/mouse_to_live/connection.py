__author__ = 'Guillaume'

from src.generic.connector import *
from src.generic.osc_API import *
from mouse_API import *
from osc_live_API import *

class MouseToLiveConnector(Connector):

    def __init__(self,ip,port,out_port,reading_FS,recording_FS=1000.,predict_online=False,record=True,session_name='test_MouseToLive',user_name="default"):

        self.recording_FS = recording_FS
        self.reading_FS = reading_FS

        self.cmd_file_type = 'mouse'
        self.out_file_type = 'osc'

        # Receive data
        self.init_reception(MouseReceiver(),OSCreceiver(ip,port))
        self.init_data_reading(MouseVectoriser(reading_FS),OscLiveVectoriser(reading_FS))

        # For recording of the performance or training create vectorisers and recorders
        if predict_online:
            self.init_online_prediction(MouseVectoriser(recording_FS),OscLiveVectoriser(recording_FS),OSCsender(ip,out_port))

        if record:
            self.init_recording(session_name,user_name)


        M_freq_list = 5
        n_full = 7 # this is seven because is does not count the expansion
        channel_to_exp = [0,1]
        n = len(channel_to_exp)
        tau_max = .2
        tau_min = 2. / reading_FS

        #expander = ScatteringExpander(n_full,tau_max,M_freq_list,tau_max,tau_min,to_expand_channel_list=channel_to_exp)
        expander = FrequencyExpander(n_full,M_freq_list,tau_max,tau_min,mem_ratio=2,to_expand_channel_list=channel_to_exp)

        self.init_classification(SVR(),expander)
