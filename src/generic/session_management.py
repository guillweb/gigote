__author__ = 'Guillaume'

from os import path,mkdir
from time import strftime, gmtime, time
import json

class Session(object):

    def __init__(self,Name,Type,User,path='sessions'):
        self.meta = lambda: None

        self.meta.name = Name
        self.meta.type = Type
        self.meta.user = User
        self.meta.date = strftime("%d_%b_%Y", gmtime())
        self.meta.day_time = strftime("%H_%M_%S", gmtime())
        self.meta.second = time()
        self.meta.mode = ''
        self.root_path = path

        self.id = 0
        self.set_path()

    def get_short_name(self):
        return self.meta.name + ("%d" % self.id).zfill(5)

    def set_path(self):

        self.path = self.root_path + "/" + self.get_short_name() + "/"
        while path.exists(self.path):
            self.id += 1
            self.path = self.root_path + "/" + self.get_short_name() + "/"

        mkdir(self.path)

        return self.path

    def save(self):
        full_name = self.path + 'session.json'

        self.file = open(full_name,'w')
        json.dump(self.meta.__dict__,self.file)
        self.file.close()

    def sync(self, receiver, recorder, suffix):
        meta = receiver.get_meta()
        self.meta.osc_meta = meta

        recorder.set_path(self.path)
        recorder.new_file(self.get_short_name() + "." + suffix)

