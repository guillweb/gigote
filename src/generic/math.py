__author__ = 'Guillaume'

import sklearn.decomposition.pca as PCA
from sklearn.linear_model.stochastic_gradient import SGDRegressor
import numpy as np
from scipy.stats import norm


class Scaler(object):
    def __init__(self,n_full,to_expand_channel_list=[]):
        """
        Init and train the expander. It learns the scaling of the input data and define the expander parameters.
        :param X:
        :param N_tau:
        :param M_freq:
        :param tau_max:
        :param FS:
        :return:
        """


        self.init_scaler(n_full,to_expand_channel_list)

    def init_scaler(self,n_full,to_expand_channel_list):

        if len(to_expand_channel_list) == 0:
            to_expand_channel_list = np.array(range(n_full))

        self.n_full = n_full
        self.ignore_list = np.setdiff1d(range(n_full),to_expand_channel_list)
        self.ch_list = to_expand_channel_list
        self.n = len(self.ch_list)

        self.X_mean = 0
        self.X_std = 0
        self.K_trained = 0

        # After expansion info

    def split_ignored_check_dim(self,x_mat_full):
        self.dim_check_full(x_mat_full)
        x_mat_ignore = x_mat_full[self.ignore_list,:]
        x_mat = x_mat_full[self.ch_list,:]

        return x_mat,x_mat_ignore

    def partial_learn_scaling(self,x_mat_full,t_mat=[]):
        x_mat,x_mat_ignore = self.split_ignored_check_dim(x_mat_full)

        # Before expansion info
        n = self.n
        K = np.shape(x_mat)[1]

        if K > 0:

            self.X_mean = (self.K_trained * self.X_mean + K * np.mean(x_mat,axis=1)) / (K + self.K_trained)
            self.X_std = np.sqrt(np.maximum( self.K_trained * self.X_std**2 + K * np.var(x_mat,axis=1),10**-10)) / np.sqrt( K + self.K_trained)
            self.K_trained += K

    def scale(self,x,epsi=10**-8):
        x = self.dim_check(x)
        xx = ((x.T - self.X_mean) / (self.X_std + epsi)).T

        return xx

    def dim_check_full(self,x_mat_full):

        if len(np.shape(x_mat_full))>1:
            K = np.shape(x_mat_full)[1]
            n_full = np.shape(x_mat_full)[0]
        else:
            K = 1
            n_full = self.n_full

        if n_full != self.n_full:
            raise ValueError("Dimension mismatch during scaling. n_full defined as {0}, array given is size {1}.".format(self.n_full,n_full))

        x_mat_full = np.reshape(x_mat_full,(n_full,K))
        return x_mat_full

    def dim_check(self,x_mat):

        if len(np.shape(x_mat))>1:
            K = np.shape(x_mat)[1]
            n = np.shape(x_mat)[0]
        else:
            K = 1
            n = self.n

        if n != self.n:
            print "Dimension mismatch during scaling of selected channels only. Should be {0}, it's {1}.".format(self.n,n)
            raise ValueError()

        x_mat = np.reshape(x_mat,(n,K))
        return x_mat

class MultiplicativeExpander(Scaler):

    def __init__(self,n_full,to_expand_ch_list=[],complex=True):
        self.init_scaler(n_full,to_expand_ch_list)
        self.n_out = (self.n) * (self.n +1)/2

    def expand_data(self,x_mat_full,t_mat=[]):

        self.dim_check_full(x_mat_full)
        x_mat,x_mat_ignore = self.split_ignored_check_dim(x_mat_full)

        if complex:
            X_mat = np.zeros((self.n_out, np.shape(x_mat)[1]), dtype=complex)
        else:
            X_mat = np.zeros((self.n_out, np.shape(x_mat)[1]))
        kk=0
        for i in range(self.n):
            for j in range(i+1):
                re = np.multiply(x_mat[i,:].real,x_mat[j,:].real)
                im = np.multiply(x_mat[i,:].imag,- x_mat[j,:].imag)
                X_mat[kk,:] = np.complex(1,0) * re + np.complex(0,1) * im
                kk+=1

        return np.vstack((x_mat_ignore,X_mat))

class SpatialQuantizer(Scaler):

    def __init__(self,n_full,k_points,to_expand_channel_list):

        self.init_scaler(n_full,to_expand_channel_list)
        self.p = k_points
        self.n_out = self.n * self.p

        vec = (np.array(range(k_points)) + 1.)/ float(k_points+1)
        self.cluster_centers = norm.isf(vec)

        vec_std = (np.array(range(2*k_points +1)) + 1.)/ float(2*k_points+1)
        a = norm.isf(vec_std)[range(0,2*k_points,2)]
        b = norm.isf(vec_std)[range(1,2*k_points+1,2)]
        self.cluster_var = np.power((a - b)*2,2)
        pass

    def quantize(self,x):
        x = self.scale(x)
        X = np.zeros(self.n_out)
        for ii in range(self.n):
            dist2 = np.power(x[ii] - self.cluster_centers,2)
            X[ii*self.p:(ii+1)*self.p] = 1./ np.sqrt(2 * np.pi * self.cluster_var) * np.exp( - dist2/(2*self.cluster_var))

        return X

    def expand_data_offline(self,x_mat_full,t_mat=[]):
        x_mat,x_mat_ignore = self.split_ignored_check_dim(x_mat_full)
        _,K = np.shape(x_mat_full)

        # Build the expanded and convolved version  of x: X_mat and init temprary vector to 0
        X_mat = np.zeros((self.n_out,K))

        # Init diffusion memory vector

        for k in range(0,K):
            X_mat[:,k] = self.quantize(x_mat[:,k])

        return np.vstack((x_mat_ignore,X_mat))


    def expand_data_online(self,x_mat_full,t=0,t_last=0):
        x_mat,x_mat_ignore = self.split_ignored_check_dim(x_mat_full)

        X_mat = np.zeros((self.n_out,0))
        X_mat[:,0] = self.quantize(x_mat[:,0])

        return np.vstack((x_mat_ignore,X_mat))


class ExpSmoother(Scaler):
    def __init__(self,n_full,tau_list,to_expand_channel_list=[],no_scaling=False):
        """
        Init and train the expander. It learns the scaling of the input data and define the expander parameters.
        """

        if not(isinstance(tau_list, list)):
            tau_list= [tau_list]

        self.init_expander(n_full,to_expand_channel_list,no_scaling=no_scaling)
        self.p = len(tau_list)
        self.n_out = self.n * self.p
        self.tau_list = np.array(tau_list)
        self.X_tmp = np.complex(1,0) * np.zeros((self.n_out,))

        # After expansion info

    @staticmethod
    def get_tau_list(tau_min,tau_max,N_tau):
        tau_list = np.exp(np.array(range(N_tau)) * np.log(tau_max/tau_min) / (N_tau-1)) * tau_min
        return tau_list

    def init_expander(self,n_full,ch_list,no_scaling=False):
        self.init_scaler(n_full,ch_list)
        self.no_scaling = no_scaling

    def get_al(self,Delta_t):
        return np.exp(- Delta_t / self.tau_list)

    def diffuse(self,x,t,t_last):
        if not(self.no_scaling):
            x = self.scale(x)

        p = self.p
        al = self.get_al(t - t_last)
        for i in range(self.n):

            self.X_tmp[i*p:(i+1)*p] *= al
            self.X_tmp[i*p:(i+1)*p] += x[i]

        return self.X_tmp

    def return_with_complex_options(self,x_mat_ignore,X_mat,complex,absolute):

        if complex:
            return np.vstack((x_mat_ignore,X_mat))
        elif absolute:
            return np.vstack((x_mat_ignore,np.abs(X_mat)))
        else:
            return np.vstack((x_mat_ignore,np.real(X_mat)))

    def expand_data_offline(self,x_mat_full,t_mat,complex=False,absolute=False):

        x_mat,x_mat_ignore = self.split_ignored_check_dim(x_mat_full)
        _,K = np.shape(x_mat_full)

        # Build the expanded and convolved version  of x: X_mat and init temprary vector to 0
        X_mat = np.complex(1,0) * np.zeros((self.n_out,K))
        if hasattr(self,'X_tmp'):self.X_tmp[:] = np.complex(1,0) * np.zeros((self.n_out,))

        # Init diffusion memory vector

        t = t_mat[0]
        X_mat[:,0] = self.diffuse(x_mat[:,0],t,t)
        for k in range(1,K):
            t_last = t
            t = t_mat[k]
            X_mat[:,k] = self.diffuse(x_mat[:,k],t,t_last)

        return self.return_with_complex_options(x_mat_ignore,X_mat,complex,absolute)

    def expand_and_diffuse_online(self,x_mat_full,t,t_last,complex=False,absolute=False):
        x_mat,x_mat_ignore = self.split_ignored_check_dim(x_mat_full)

        # Diffuse
        X_mat = np.complex(1,0) * np.zeros((self.n_out,1))
        X_mat[:,0] = self.diffuse(x_mat[:,0],t,t_last)
        return self.return_with_complex_options(x_mat_ignore,X_mat,complex,absolute)

# Time frequency representation
class FrequencyExpander(ExpSmoother):
    def __init__(self,n_full,M_freq,tau_max,tau_min,mem_ratio=8,to_expand_channel_list=[]):
        """
        Init and train the expander. It learns the scaling of the input data and define the expander parameters.
        """

        self.init_expander(n_full,to_expand_channel_list)

        self.n_out = self.n * M_freq
        self.tau_max = tau_max
        self.tau_min = tau_min
        self.mem_ratio = mem_ratio

        self.M_freq = M_freq
        self.p = self.M_freq

        tau_list = ExpSmoother.get_tau_list(tau_min,tau_max,M_freq)
        self.freq_list = 1. / tau_list

        self.X_tmp = np.zeros((self.n * M_freq,), dtype=complex)

        # After expansion info

    def get_al(self,Delta_t):
        if self.mem_ratio >0:
            tau_mem = 1. / self.freq_list * self.mem_ratio
            mod = np.exp(- Delta_t / tau_mem)
        else:
            mod = 1.

        ph = 2*np.pi * self.freq_list * Delta_t
        comp = np.complex(1,0) * np.cos(ph) + np.complex(0,1) * np.sin(ph)
        return np.multiply(mod,comp)

class ScatteringExpander(ExpSmoother):

    def __init__(self,n_full,tau_int,M_freq_list,tau_max,tau_min,to_expand_channel_list=[]):

        self.N_layer = len(M_freq_list)
        self.init_expander(n_full,to_expand_channel_list)

        self.high_passer = MovingAverageHighPasser(self.n,tau_max,to_expand_channel_list)
        self.expander_0 = FrequencyExpander(self.high_passer.n_out,M_freq_list[0],tau_max,tau_min,mem_ratio=8)
        self.n_out = self.expander_0.n_out

        if self.N_layer == 2:
            self.expander_1 = FrequencyExpander(self.expander_0.n_out,M_freq_list[1],tau_max,tau_min,mem_ratio=8)
            self.n_out = self.expander_0.n_out + self.expander_1.n_out

        self.smoother = ExpSmoother(self.n_out,tau_int)

        self.out_scaler = Scaler(self.smoother.n_out)

    def partial_learn_scaling(self,x_mat_full,t_mat):

        x_mat_full = self.dim_check_full(x_mat_full)
        x_mat_ignore = x_mat_full[self.ignore_list,:]
        x_mat = x_mat_full[self.ch_list,:]

        self.high_passer.partial_learn_scaling(x_mat)
        X = self.high_passer.expand_data_offline(x_mat,t_mat)

        self.expander_0.partial_learn_scaling(X)
        X = self.expander_0.expand_data_offline(x_mat,t_mat,absolute=True)

        # Expand the second layer
        if self.N_layer == 2:
            self.expander_1.partial_learn_scaling(X)
            X_1 = self.expander_1.expand_data_offline(X,t_mat,absolute=True)
            X = np.vstack((X,X_1))

        # Smooth the whole
        self.smoother.partial_learn_scaling(X)
        X_smoothed = self.smoother.expand_data_offline(X,t_mat)

        self.out_scaler.partial_learn_scaling(X_smoothed)

    def expand_data_offline(self,x_mat_full,t_mat):

        x_mat_full = self.dim_check_full(x_mat_full)
        x_mat_ignore = x_mat_full[self.ignore_list,:]
        x_mat = x_mat_full[self.ch_list,:]

        X = self.high_passer.expand_data_offline(x_mat,t_mat)
        X = self.expander_0.expand_data_offline(X,t_mat,absolute=True)

        if self.N_layer == 2:
            X_1 = self.expander_1.expand_data_offline(X,t_mat,absolute=True)
            X = np.vstack((X,X_1))

        # Smooth the whole
        X = self.smoother.expand_data_offline(X,t_mat)
        X = self.out_scaler.scale(X)

        return np.vstack((x_mat_ignore,X))

    def expand_and_diffuse_online(self,x_mat_full,t,t_last,complex=False,absolute=False):

        x_mat_full = self.dim_check_full(x_mat_full)
        x_mat_ignore = x_mat_full[self.ignore_list,:]
        x_mat = x_mat_full[self.ch_list,:]

        # Diffuse
        X = self.high_passer.expand_and_diffuse_online(x_mat,t,t_last)
        X = self.expander_0.expand_and_diffuse_online(X,t,t_last,absolute=True)
        if self.N_layer == 2:
            X_1 = self.expander_1.expand_and_diffuse_online(X,t,t_last,absolute=True)

        X = np.vstack((X,X_1))
        X = self.smoother.expander_and_diffuse_online(X)
        X = self.out_scaler.scale(X)

        return np.vstack((x_mat_ignore,X))

class SeparableScatExpander(ExpSmoother):

    def __init__(self,n_full,tau_int,M_freq,tau_max,tau_min,to_expand_channel_list=[]):

        self.init_scaler(n_full,to_expand_channel_list)

        self.expander = FrequencyExpander(self.n,M_freq,tau_max,tau_min,mem_ratio=8)

        self.multiplicater = MultiplicativeExpander(self.expander.n_out)
        self.n_out = self.multiplicater.n_out + self.expander.n_out

        self.smoother = ExpSmoother(self.n_out,tau_int)

    def partial_learn_scaling(self,x_mat_full,t_mat):

        x_mat_full = self.dim_check_full(x_mat_full)
        x_mat_ignore = x_mat_full[self.ignore_list,:]
        x_mat = x_mat_full[self.ch_list,:]

        self.expander.partial_learn_scaling(x_mat)
        X = self.expander.expand_data_offline(x_mat,t_mat,complex=True)

        # Expand the second layer
        self.multiplicater.partial_learn_scaling(X)
        X_m = self.multiplicater.expand_data(X)

        X = np.abs(np.vstack((X,X_m)))
        # Smooth the whole
        self.smoother.partial_learn_scaling(X)

    def expand_data_offline(self,x_mat_full,t_mat):

        x_mat_full = self.dim_check_full(x_mat_full)
        x_mat_ignore = x_mat_full[self.ignore_list,:]
        x_mat = x_mat_full[self.ch_list,:]

        X = self.expander.expand_data_offline(x_mat,t_mat,complex=True)

        # Smooth the whole
        X_m = self.multiplicater.expand_data(X)
        X = np.abs(np.vstack((X,X_m)))
        X = self.smoother.expand_data_offline(X,t_mat)

        return np.vstack((x_mat_ignore,X))

    def expand_and_diffuse_online(self,x_mat_full,t,t_last):

        x_mat_full = self.dim_check_full(x_mat_full)
        x_mat_ignore = x_mat_full[self.ignore_list,:]
        x_mat = x_mat_full[self.ch_list,:]

        # Diffuse
        X = self.expander.expand_and_diffuse_online(x_mat,t,t_last,complex=True)
        X_m = self.multiplicater.expand_data(X)

        X = np.abs(np.vstack((X,X_m)))
        X = self.smoother.expand_and_diffuse_online(X,t,t_last)

        return np.vstack((x_mat_ignore,X))


    #

# High passers
class MovingAverageHighPasser(ExpSmoother):

    def diffuse(self,x,t,t_last):
        x = self.scale(x).flatten()
        self.X_tmp *= np.real(self.get_al(t - t_last))
        self.X_tmp += x

        return x - self.X_tmp

class PCAProjectionHighPasser(MovingAverageHighPasser):

    def __init__(self,n_full,tau_list,to_expand_channel_list=[]):
        MovingAverageHighPasser.__init__(self,n_full,tau_list,to_expand_channel_list=[])
        self.X_PCA = np.zeros((self.n,))


    def diffuse(self,x,t,t_last):
        x = MovingAverageHighPasser.diffuse(self,x,t,t_last)

        s = np.dot(self.X_PCA,x)
        proj = self.X_PCA * s

        self.X_PCA *= self.get_al(t - t_last)
        self.X_PCA += x * s
        self.X_PCA /= np.linalg.norm(self.X_tmp)

        return x - proj

class PCAAngleHighPasser(PCAProjectionHighPasser):

    def __init__(self,n_full,tau,to_expand_channel_list=[]):
        self.init_scaler(n_full,to_expand_channel_list)
        self.p = 1
        self.n_out = 1
        self.tau_list = [tau]
        self.X_tmp = np.complex(0,1) * np.zeros((self.n,))
        self.X_PCA = np.complex(0,1) * np.zeros((self.n,))

    def diffuse(self,x,t,t_last,epsi=10**-6):
        x = MovingAverageHighPasser.diffuse(self,x,t,t_last)

        s = np.dot(self.X_PCA.real,x.real)

        self.X_PCA *= self.get_al(t - t_last)
        self.X_PCA += x * s
        self.X_PCA /= (np.linalg.norm(self.X_PCA) + epsi)

        return s / (np.linalg.norm(x) + epsi)

# Enveloppe extractor
# TODO: Avoid useless complex numbers.
class EnvelopeExtractor(ExpSmoother):
    def __init__(self,n_full,tau_list,to_expand_channel_list=[],no_scaling=True):
        self.init_expander(n_full,to_expand_channel_list,no_scaling=no_scaling)
        self.p = len(tau_list)
        self.n_out = 1
        self.tau_list = tau_list
        self.X_tmp = np.complex(0,1) *  np.zeros((self.p,self.n))

    def diffuse(self,x,t,t_last):
        x = x.flatten()

        p = self.p
        al = self.get_al(t - t_last)

        for i in range(self.n):

            self.X_tmp[:,i] *= al
            self.X_tmp[:,i] += x[i]

        sq = np.mean(np.sum(np.power(self.X_tmp,2), axis=1))

        return sq

# Acc data treatment
class AccPreprocesser(ExpSmoother):
    def __init__(self,n_full,tau_max,tau_min,N_tau,to_expand_channel_list=[]):
        self.init_expander(n_full,to_expand_channel_list)
        if not(self.n == 3):
            ValueError('For accleremoter data should be initialised to 3. {0} given.'.format(self.n))

        tau_list = ExpSmoother.get_tau_list(tau_min,tau_max,N_tau)
        self.envelope_extractor = EnvelopeExtractor(3,[tau_min],no_scaling=True)
        self.angle_extractor = PCAAngleHighPasser(3,tau_max)
        self.env_exp = FrequencyExpander(1,N_tau,tau_max,tau_min,mem_ratio=4)
        self.ang_exp = FrequencyExpander(1,N_tau,tau_max,tau_min,mem_ratio=4)

        self.n_out = self.env_exp.n_out + self.ang_exp.n_out

    def diffuse(self,x,t,t_last):
        x_env = self.envelope_extractor.diffuse(x,t,t_last)
        x_ang = self.angle_extractor.diffuse(x,t,t_last)

        X_env = self.env_exp.diffuse(x_env,t,t_last)
        X_ang = self.ang_exp.diffuse(x_ang,t,t_last)

        return np.hstack((X_env,X_ang))

    def partial_learn_scaling(self,x_mat_full,t_mat=[]):
        x_mat,x_mat_ignore = self.split_ignored_check_dim(x_mat_full)

        self.envelope_extractor.partial_learn_scaling(x_mat,t_mat)
        self.angle_extractor.partial_learn_scaling(x_mat,t_mat)

        x_env = self.envelope_extractor.expand_data_offline(x_mat,t_mat)
        x_ang = self.angle_extractor.expand_data_offline(x_mat,t_mat)

        self.env_exp.partial_learn_scaling(x_env)
        self.ang_exp.partial_learn_scaling(x_ang)
